#!/bin/bash

HTMLOUTPUT=/opt/iotproject/index.html
DIRECTORYTOLIST=/tmp
INTERVAL=20

if [ -n "$1" ]; then
    if [ -d "$1" ]; then
        DIRECTORYTOLIST=$1
    else
        echo "Directory $1 not found!"
        exit 1
    fi
fi
shift

counter=0
while [ $counter -lt 20 ]; do
    echo "<html><body>" > $HTMLOUTPUT
    echo "<h1>Mein Webserver</h1>" >> $HTMLOUTPUT
    date +%H:%M:%S >> $HTMLOUTPUT
    ls $DIRECTORYTOLIST >> $HTMLOUTPUT

    for htmlsnippets in $*
    do
        if [ -r $htmlsnippets ]; then
            cat $htmlsnippets >> $HTMLOUTPUT
        else
            echo "File $htmlsnippets is not readable!" >&2
        fi
    done

    echo "</body></html>" >> $HTMLOUTPUT
    sleep $INTERVAL
    ((counter++))
done